output "jenkins_service_dns" {
  value = module.eks.jenkins_service_dns
}
output "jenkins_full_url" {
  value = module.eks.jenkins_full_url
}
