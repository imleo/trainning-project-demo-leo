variable "vpc_id" {}
variable "subnet_cidr_block" {}
variable "route_table_id" {}
variable "public_subnet" {}
variable "deploy_nat" {}
variable "availability_zone" {}
variable "nat_id" {
  default = ""
}
variable "tags" {
  default = {}
}
