output "subnet_id" {
  value = aws_subnet.main.id
}

output "nat_gateway_id" {
  value = var.deploy_nat ? aws_nat_gateway.nat[0].id:""
}
