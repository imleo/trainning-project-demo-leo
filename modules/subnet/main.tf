resource "aws_subnet" "main" {
  vpc_id                  = var.vpc_id
  cidr_block              = var.subnet_cidr_block
  map_public_ip_on_launch = var.public_subnet

  availability_zone       = var.availability_zone

  tags = var.tags
}

resource "aws_route_table_association" "route_table_association"{
  subnet_id       = aws_subnet.main.id
  route_table_id  = var.route_table_id
}
