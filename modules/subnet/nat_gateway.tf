resource "aws_eip" "nat" {
  count = var.deploy_nat ? 1:0

  vpc   = true
}

resource "aws_nat_gateway" "nat" {
  count         = var.deploy_nat ? 1:0

  allocation_id = aws_eip.nat[0].id
  subnet_id     = aws_subnet.main.id
}

resource "aws_route" "route_nat_gateway" {
  count                   = var.deploy_nat ? 0:1

  destination_cidr_block  = "0.0.0.0/0"
  route_table_id          = var.route_table_id
  nat_gateway_id          = var.nat_id
}
