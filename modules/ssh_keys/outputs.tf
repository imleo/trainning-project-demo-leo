output "private_pem" {
  value       = tls_private_key.ssh_training_key.private_key_pem
}

output "key_name" {
  value       = aws_key_pair.ssh_training_generated_key.key_name
}
