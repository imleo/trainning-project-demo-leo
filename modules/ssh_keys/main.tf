resource "tls_private_key" "ssh_training_key" {
  algorithm = "RSA"
  rsa_bits  = 4096

  provisioner "local-exec" {
    command     = "echo \"${tls_private_key.ssh_training_key.private_key_pem}\" > ${var.ssh_key_name} && chmod 600 ${var.ssh_key_name}"
    interpreter = ["bash", "-c"]
  }
}

resource "aws_key_pair" "ssh_training_generated_key" {
  key_name   = var.ssh_key_name
  public_key = tls_private_key.ssh_training_key.public_key_openssh
}
