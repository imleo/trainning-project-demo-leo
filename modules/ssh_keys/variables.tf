# providers
variable "aws_default_region" {
  type = string
  default = "us-west-2"
}

variable "ssh_key_name" {
  type = string
  default = "ssh_created_key"
}
