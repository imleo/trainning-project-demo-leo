resource "github_repository_webhook" "webhook" {
  repository  = var.github_repository

  configuration {
    url          = var.webhook_url
    content_type = "form"
    insecure_ssl = var.secure_webhook
  }

  active = true

  events = ["push"]
}
