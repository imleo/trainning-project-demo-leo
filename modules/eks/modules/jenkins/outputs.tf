output "jenkins_service_dns" {
  value = data.kubernetes_service.jenkins_service.status.0.load_balancer.0.ingress.0.hostname
}
output "jenkins_route53_record" {
  value = var.domain != "" ? aws_route53_record.jenkins_record[0].name:""
}
output "webhook_url" {
  value = local.jenkins_webhook
}
output "secure_webhook" {
  value = var.secure_webhook
}
output "jenkins_full_url"{
  value = local.full_jenkins_url
}
