data "aws_route53_zone" "domain" {
  count = var.domain != "" ? 1:0

  name  = "${ var.domain }."
}

resource "aws_route53_record" "jenkins_record" {
  count   = var.domain != "" ? 1:0

  zone_id = data.aws_route53_zone.domain[0].zone_id

  name    = local.record_name
  type    = "CNAME"
  ttl     = 100

  records = [
    data.kubernetes_service.jenkins_service.status.0.load_balancer.0.ingress.0.hostname
  ]
}

###################################################### certificate
resource "aws_acm_certificate" "jenkins" {
  count   = var.secure_webhook ? 1:0

  domain_name       = local.record_name
  validation_method = "DNS"
}

resource "aws_route53_record" "jenkins_cert_validation" {
  for_each = var.secure_webhook == false ? {}:{
    for dvo in aws_acm_certificate.jenkins[0].domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = data.aws_route53_zone.domain[0].zone_id
}

resource "aws_acm_certificate_validation" "jenkins_validation" {
  count   = var.secure_webhook ? 1:0

  certificate_arn         = aws_acm_certificate.jenkins[0].arn
  validation_record_fqdns = [for record in aws_route53_record.jenkins_cert_validation : record.fqdn]
}
