variable "jenkins_fullname" {
  default = "jenkins"
}

variable "jenkins_namespace" {
  default = "jenkins"
}

variable "domain" {
  default = ""
}

variable "github_user" {
  default = ""
}

variable "github_repo" {
  default = ""
}

variable "github_token" {
  default = ""
}

variable "secure_webhook" {
  default = false
}

locals {
  record_name       = "lc.jenkins.${ var.domain }"
  jenkins_protocol  = var.domain != "" && var.secure_webhook ? "https":"http"
  full_jenkins_url  = "${ local.jenkins_protocol }://${ local.record_name }"
  jenkins_webhook   = "${ local.full_jenkins_url }/github-webhook/"
}
