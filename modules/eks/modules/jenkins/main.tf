data "template_file" "jenkins_values" {
  template              = file("${path.module}/values.yaml")
  vars = {
    fullname            = var.jenkins_fullname
    jenkins_web_url     = local.full_jenkins_url
    jenkins_webhook_url = local.jenkins_webhook
    certificate_arn     = var.secure_webhook ? aws_acm_certificate.jenkins[0].arn:""
    github_user         = var.github_user
    github_token        = var.github_token
    github_repo         = var.github_repo
  }
}

resource "helm_release" "jenkins" {
  name              = "jenkins"
  repository        = "https://charts.jenkins.io"
  chart             = "jenkins"
  version           = "3.1.8"

  create_namespace  = true
  namespace         = var.jenkins_namespace

  values            = [ data.template_file.jenkins_values.rendered ]
}

data "kubernetes_service" "jenkins_service" {
  depends_on  = [ helm_release.jenkins ]

  metadata {
    name      = var.jenkins_fullname
    namespace = var.jenkins_namespace
  }
}
