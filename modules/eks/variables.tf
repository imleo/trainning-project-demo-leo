variable "cluster_name" {}
variable "subnet_ids" {}
variable "cluster_version" {}

variable "vpc_id" {}
variable "public_subnets" {}

# variable "domain" {}
variable "manage_worker_iam_resources" {
  default = true
}

variable "encrypt_secrets" {
  default = false
}

variable "deploy_jenkins"{
  default = false
}
variable "jenkins_fullname" {
  default = "jenkins"
}
variable "jenkins_namespace" {
  default = "jenkins"
}

variable "worker_groups" {
  default = []
}
variable "fargate_profiles" {
  default = []
}

variable "domain" {
  default = ""
}

variable "github_user" {
  default = ""
}

variable "github_repo" {
  default = ""
}

variable "github_token" {
  default = ""
}

locals {
  eks_config_output_path = "${path.module}/${var.cluster_name}_config"
}
