output "jenkins_route53_record" {
  value = var.deploy_jenkins ? module.jenkins[0].jenkins_route53_record:""
}
output "jenkins_service_dns" {
  value = var.deploy_jenkins ? module.jenkins[0].jenkins_service_dns:""
}
output "jenkins_full_url" {
  value = var.deploy_jenkins ? module.jenkins[0].jenkins_full_url:""
}
