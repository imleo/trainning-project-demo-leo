resource "aws_kms_key" "eks_encrypt" {
  count       = var.encrypt_secrets ? 1:0
  description = "EKS encryption key"
}

module "eks" {
  source                      = "terraform-aws-modules/eks/aws"
  version                     = "14.0.0"

  cluster_name                = var.cluster_name
  cluster_version             = var.cluster_version
  write_kubeconfig            = true
  config_output_path          = local.eks_config_output_path

  vpc_id                      = var.vpc_id
  subnets                     = var.subnet_ids

  manage_worker_iam_resources = var.manage_worker_iam_resources
  worker_groups               = var.worker_groups

  fargate_profiles            = var.fargate_profiles

  cluster_encryption_config   = var.encrypt_secrets ? [
    {
      provider_key_arn = aws_kms_key.eks_encrypt[0].arn
      resources        = ["secrets"]
    }
  ]:[]
}

module "jenkins" {
  source            = "./modules/jenkins"

  count             = var.deploy_jenkins ? 1:0
  depends_on        = [ module.eks ]

  jenkins_fullname  = var.jenkins_fullname
  jenkins_namespace = var.jenkins_namespace
  domain            = var.domain
  github_user       = var.github_user
  github_token      = var.github_token
  github_repo       = var.github_repo
  secure_webhook    = false
}

module "github_webhooks" {
  source            = "./modules/github_repo_webhook"

  depends_on        = [ module.jenkins ]

  github_repository = var.github_repo
  webhook_url       = module.jenkins[0].webhook_url
  secure_webhook    = module.jenkins[0].secure_webhook
}
