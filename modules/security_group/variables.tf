variable "security_group_name" {}
variable "vpc_id" {}

variable "ingress_rules" {
  default = {}
}

variable "egress_rules" {
  default = {}
}
