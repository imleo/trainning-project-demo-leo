resource "aws_vpc" "vpc" {
  cidr_block        = var.vpc_cidr_block

  instance_tenancy  = "default"

  tags = {
    Name = var.vpc_name
  }
}

resource "aws_internet_gateway" "gateway" {
  vpc_id = aws_vpc.vpc.id
}
