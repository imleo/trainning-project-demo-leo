output "vpc_id" {
  value       = aws_vpc.vpc.id
}

output "gateway_id" {
  value = aws_internet_gateway.gateway.id
}

output "cidr_block" {
  value = aws_vpc.vpc.cidr_block
}

output "default_route_table_id" {
  value = aws_vpc.vpc.default_route_table_id
}

output "extra_route_tables_id" {
  value = aws_route_table.extra_route_tables.*.id
}
