resource "aws_route" "route_internet_gateway" {
  destination_cidr_block  = "0.0.0.0/0"
  route_table_id          = aws_vpc.vpc.default_route_table_id
  gateway_id              = aws_internet_gateway.gateway.id
}

resource "aws_route_table" "extra_route_tables" {
  count  = var.extra_route_tables

  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "route-table-${ count.index }"
  }
}
