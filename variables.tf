variable "aws_default_region" {
  default = "us-west-2"
}

variable "vpc_cidr_block" {
  default = "10.0.0.0/16"
}

variable "key_name" {
  default = "ssh_training_key"
}

variable "eks_cluster_name" {
  default = "lc-cluster"
}

variable "domain" {
  default = ""
}

variable "github_user" {
  default = ""
}

variable "github_repo" {
  default = ""
}

variable "github_token" {
  default = ""
}
