terraform {
  backend "local" {
    path          = "terraform.tfstate"
    workspace_dir = "workspace_test"
  }

  required_version = ">= 0.14.3, < 0.15.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 2.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "~> 2.0.2"
    }
    github = {
      source  = "hashicorp/github"
      version = "~> 4.4.0"
    }
    local = {
      source  = "hashicorp/local"
      version = "~> 2.0"
    }
  }
}



data "aws_availability_zones" "available" {
  state = "available"
}




module "vpc" {
  source = "./modules/vpc"

  vpc_name           = "lc-vpc-test-1"
  vpc_cidr_block     = var.vpc_cidr_block
  extra_route_tables = 2
}

module "public_subnets" {
  source = "./modules/subnet"
  count  = 2

  public_subnet = true
  deploy_nat    = true

  vpc_id            = module.vpc.vpc_id
  subnet_cidr_block = cidrsubnet(module.vpc.cidr_block, 8, count.index)
  route_table_id    = module.vpc.default_route_table_id

  availability_zone = data.aws_availability_zones.available.names[count.index]

  tags = {
    "Name"                                          = "lc-public-subnet-${count.index}"
    "kubernetes.io/cluster/${var.eks_cluster_name}" = "shared"
    "kubernetes.io/role/elb"                        = "1"
  }
}

module "private_subnets" {
  source = "./modules/subnet"
  count  = 2

  public_subnet = false
  deploy_nat    = false

  nat_id = count.index >= length(module.public_subnets) ? module.public_subnets[length(module.public_subnets) - 1].nat_gateway_id : module.public_subnets[count.index].nat_gateway_id

  vpc_id            = module.vpc.vpc_id
  subnet_cidr_block = cidrsubnet(module.vpc.cidr_block, 8, sum([length(module.public_subnets), count.index]))
  route_table_id    = module.vpc.extra_route_tables_id[count.index]

  availability_zone = data.aws_availability_zones.available.names[count.index]

  tags = {
    "Name"                                          = "lc-private-subnet-${count.index}"
    "kubernetes.io/cluster/${var.eks_cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"               = "1"
  }
}

module "security_group" {
  source = "./modules/security_group"

  vpc_id              = module.vpc.vpc_id
  security_group_name = "lc-security-demo-test"

  ingress_rules = {
    ssh = {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  egress_rules = {
    ssh = {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
}

module "eks_worker_instance_profile" {
  source = "./modules/iam"

  name   = "lc-eks-worker"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "*",
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

module "eks" {
  source = "./modules/eks"

  vpc_id = module.vpc.vpc_id

  encrypt_secrets = true

  cluster_name    = var.eks_cluster_name
  cluster_version = "1.18"

  subnet_ids     = module.private_subnets.*.subnet_id
  public_subnets = module.public_subnets.*.subnet_id

  manage_worker_iam_resources = true
  worker_groups = [
    {
      name                          = "worker-group-1"
      instance_type                 = "t3.small"
      asg_desired_capacity          = 2
      additional_security_group_ids = [module.security_group.security_group_id]
      root_volume_type              = "gp2"
      # iam_instance_profile_name     = module.eks_worker_instance_profile.profile_id
    }
  ]

  deploy_jenkins    = true
  jenkins_namespace = "jenkins"
  domain            = var.domain

  github_user  = var.github_user
  github_token = var.github_token
  github_repo  = var.github_repo
}
